/*
 ============================================================================
 Name        : MASTER_BRIDGE.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "common.h"

pthread_t udpListenerTid;

int udpListenerSocDes = -1;

int tcpListenerSocDes = -1;

slaveServer SLAVE_SERVERS [MAX_NUMBER_OF_SLAVE_SERVERS];

void checkSlaveServers () {
	int i;
	timevar now;
	struct in_addr addr;

	getCurrentTime (now);

	for (i = 0 ; i < MAX_NUMBER_OF_SLAVE_SERVERS ; i++) {
		if (SLAVE_SERVERS[i].ipAddrInt) {
			if (diffUsec (SLAVE_SERVERS[i].lastMsgTime, now) > (UDP_MASTER_TIMEOUT*2)) {
				addr.s_addr = SLAVE_SERVERS[i].ipAddrInt;
				printf ("checkSlaveServers - Lost the slave server : %s - %d\n", inet_ntoa(addr), SLAVE_SERVERS[i].port);
				destroySocket (SLAVE_SERVERS[i].udpResponseFilDes);
				memset (&(SLAVE_SERVERS[i]), 0, sizeof (slaveServer));
			}
		}
	}
}

int rejectionSocket = -1;

int locateSlaveServer (unsigned int ipAddrInt, unsigned short port, unsigned short udpPort) {
	int i , firstEmptyEntry = -1;
	struct in_addr addr;
	int sockDes;

	for (i = 0 ; i < MAX_NUMBER_OF_SLAVE_SERVERS ; i++) {
		if (SLAVE_SERVERS[i].ipAddrInt == ipAddrInt && SLAVE_SERVERS[i].port == port) {
			return i;
		}

		if (SLAVE_SERVERS[i].ipAddrInt == 0 && firstEmptyEntry == -1) {
			firstEmptyEntry = i;
		}
	}

	sockDes = socket (AF_INET, SOCK_DGRAM, 0);
	if (sockDes < 0) {
		printf ("locateSlaveServer - socket failed : %s\n", strerror (errno));
		exit (1);
	}

	if (firstEmptyEntry == -1) {
		rejectionSocket = sockDes;
		return -1;
	} else {
		addr.s_addr = ipAddrInt;
		printf ("locateSlaveServer - A new slave server has been discovered : %s - %d\n", inet_ntoa(addr), port);
		SLAVE_SERVERS[firstEmptyEntry].ipAddrInt         = ipAddrInt;
		SLAVE_SERVERS[firstEmptyEntry].port              = port;
		SLAVE_SERVERS[firstEmptyEntry].udpPort           = udpPort;
		SLAVE_SERVERS[firstEmptyEntry].udpResponseFilDes = sockDes;
		return firstEmptyEntry;
	}
}

void updateSlaveServerRecord (unsigned int ipAddrInt, unsigned short port, unsigned short udpPort) {
	int index, rc;

	struct sockaddr_in addrDesc;
	memset (&addrDesc, 0, sizeof (addrDesc));
	addrDesc.sin_family = AF_INET;
	addrDesc.sin_addr.s_addr = ipAddrInt;
	addrDesc.sin_port = htons (udpPort);

	index = locateSlaveServer (ipAddrInt, port, udpPort);

	if (index == -1) {
		printf ("updateSlaveServerRecord - New server (%s - %d) can not be registered, reached maximum!\n", inet_ntoa(addrDesc.sin_addr), port);
		rc = sendto (rejectionSocket, STOP_STR, CONT_STOP_STRLEN, 0, (struct sockaddr *) &addrDesc, sizeof (addrDesc));
	} else {
		getCurrentTime (SLAVE_SERVERS[index].lastMsgTime);
		rc = sendto (SLAVE_SERVERS[index].udpResponseFilDes, CONT_STR, CONT_STOP_STRLEN, 0, (struct sockaddr *) &addrDesc, sizeof (addrDesc));
	}

	if (rc != CONT_STOP_STRLEN) {
		printf ("updateSlaveServerRecord - sendto failed : %s\n", strerror (errno));
		exit (1);
	}
}

int validateMessage (unsigned char * message, int * tcpPort, int * udpPort) {
	char dummyStrBuffer [IO_BUFFER_SIZE + 1];
	if (strncmp (KEEP_ALIVE_STR, (char *)message, KEEP_ALIVE_STRLEN) == 0) {
		sscanf ((char *)message, "%s %d %d", dummyStrBuffer, tcpPort, udpPort);
		return TRUE;
	} else {
		return FALSE;
	}
}

void * udpDiscoveryListenerTaskMain (void * ARGS) {
	struct sockaddr_in addressDescriptor;
	unsigned int addrDescriptorLength = sizeof (addressDescriptor);
	int  length;
	unsigned char message [IO_BUFFER_SIZE + 1] = {0};
	int tcpPort , udpPort;

	int checkControlCounter = 0;

	while (TRUE) {
		length = recvfrom (udpListenerSocDes, message, IO_BUFFER_SIZE, 0, (struct sockaddr *) &addressDescriptor, &addrDescriptorLength);

		if (length < 0) {
			// An error occured. Report it and try to continue.
			if (errno != EAGAIN && errno != EWOULDBLOCK) {
				printf ("udpDiscoveryListenerTaskMain - recvfrom failed : %s\n", strerror (errno));
				continue;
			} else {
				// TIMEOUT occured. Check whether there is and update or not.
				checkSlaveServers ();
			}
		} else {
			message [length] = '\0';
			if (validateMessage (message, &tcpPort, &udpPort) == TRUE) {
				// Received a message from a slave server
				updateSlaveServerRecord (addressDescriptor.sin_addr.s_addr, tcpPort, udpPort);
				checkControlCounter++;
				if (checkControlCounter >= 5) {
					checkControlCounter = 0;
					checkSlaveServers ();
				}
			} else {
				printf ("udpDiscoveryListenerTaskMain - recvfrom received an invalid message : %s\n", message);
			}
		}
	}

	return NULL;
}

void destroyUdpSocket () {
	destroySocket (udpListenerSocDes);
}

void createAndBindUdpSocket () {
	struct sockaddr_in addressDescriptor;

	unsigned int addrDescriptorLength = sizeof (addressDescriptor);

	int rc;
	struct timeval timeout;

	timeout.tv_sec  = 0;
	timeout.tv_usec = UDP_MASTER_TIMEOUT;

	udpListenerSocDes = socket (AF_INET, SOCK_DGRAM, 0);
	if (udpListenerSocDes < 0) {
		printf ("createAndBindUdpSocket - socket failed : %s\n", strerror (errno));
		exit (0);
	}

	memset (&addressDescriptor, 0, sizeof (addressDescriptor));
	addressDescriptor.sin_family      = AF_INET;
	addressDescriptor.sin_port        = htons (UDP_MASTER_LISTENER_PORT);
	addressDescriptor.sin_addr.s_addr = htonl (INADDR_ANY);

	rc = bind (udpListenerSocDes, (struct sockaddr *) &addressDescriptor, addrDescriptorLength);
	if (rc < 0) {
		printf ("createAndBindUdpSocket - bind failed : %s\n", strerror (errno));
		destroyUdpSocket ();
		exit (0);
	}

	rc = setsockopt (udpListenerSocDes, SOL_SOCKET, SO_RCVTIMEO, (void *)&timeout, sizeof(timeout));
	if (rc < 0) {
		printf ("createAndBindUdpSocket - setsockopt failed : %s\n", strerror (errno));
		destroyUdpSocket ();
		exit (0);
	}

	atexit (&destroyUdpSocket);
}

void destroyTcpSocket () {
	destroySocket (tcpListenerSocDes);
}

void createAndBindTcpSocket () {
	struct sockaddr_in serverAddrDesc;
	int rc;

	tcpListenerSocDes = socket (AF_INET, SOCK_STREAM, 0);

	if (tcpListenerSocDes < 0) {
		printf ("createAndBindTcpSocket - socket failed : %s\n", strerror (errno));
		destroyTcpSocket ();
		exit (0);
	}

	memset (&serverAddrDesc, 0, sizeof (serverAddrDesc));
	serverAddrDesc.sin_family      = AF_INET;
	serverAddrDesc.sin_addr.s_addr = htonl (INADDR_ANY);
	serverAddrDesc.sin_port        = htons (TCP_MASTER_BRIDGE_PORT);

	rc = bind (tcpListenerSocDes, (struct sockaddr *) &serverAddrDesc, sizeof (serverAddrDesc));
	if (rc < 0) {
		printf ("createAndBindTcpSocket - bind failed : %s\n", strerror (errno));
		destroyTcpSocket ();
		exit (0);
	}

	rc = listen (tcpListenerSocDes, MAX_TCP_CLIENTS + 1);

	if (rc < 0) {
		printf ("createAndBindTcpSocket - accept failed : %s\n", strerror (errno));
		destroyTcpSocket ();
		exit (0);
	}

	atexit (destroyTcpSocket);
}

int main () {
	int rc;
	int clientFd;
	struct sockaddr_in clientAddrDesc;
	socklen_t clientAddrDescLen = sizeof (clientAddrDesc);

	createAndBindTcpSocket ();
	createAndBindUdpSocket ();

	memset (&SLAVE_SERVERS, 0, sizeof (SLAVE_SERVERS));

	rc = pthread_create (&udpListenerTid, NULL, &udpDiscoveryListenerTaskMain, NULL);

	if (rc < 0) {
		printf ("main - pthread_create failed : %s\n", strerror (errno));
		exit (1);
	}

	pthread_detach (udpListenerTid);

	while (TRUE) {
		clientFd = accept (tcpListenerSocDes, (struct sockaddr *) &clientAddrDesc, &clientAddrDescLen);

		rc = send (clientFd, (void *) &SLAVE_SERVERS, sizeof(SLAVE_SERVERS), 0);

		if (rc != sizeof(SLAVE_SERVERS)) {
			printf ("main - send failed : %s\n", strerror (errno));
			exit (1);
		}

		destroySocket (clientFd);
	}

	return 0;
}
